#!/bin/bash
set -e
## build ARGs
NCPUS=${NCPUS:--1}

#set -e
#apt-get update -qq && apt-get -y --no-install-recommends install \
#  rm -rf /var/lib/apt/lists/*

## plotting tools
install2.r --error --skipinstalled -n $NCPUS \
    ggplot2 \
    ggalt \
    ggsci \
    ggthemes \
    ggrepel \
    gridExtra \
    ggbeeswarm \
    ggsignif \
    ggpubr \
    ggExtra \
    ggrastr \
    gggenes \
    ggvenn \
    GGally \
    ggseqlogo \
    UpSetR \
    viridis \
    RColorBrewer \
    scales \
    igraph \
    ape \
    pheatmap \
    extrafont

installGithub.r \
   briatte/ggnet

installBioc.r \
    ggtree
    
# tables & data wrangling    
install2.r --error --skipinstalled -n $NCPUS \
    data.table \
    DT \
    stringr \
    reshape 

# General tools
install2.r --error --skipinstalled -n $NCPUS \
    amap

rm -rf /tmp/downloaded_packages