#!/bin/bash

## build ARGs
NCPUS=${NCPUS:--1}

#set -e
#apt-get update -qq && apt-get -y --no-install-recommends install \
#  rm -rf /var/lib/apt/lists/*


install2.r --error --skipinstalled -n $NCPUS \
    glmnet \
    glmnetUtils \
    e1071 \
    ggplot2 \
    ggsci \
    ggthemes \
    ggpubr \
    VGAM \
    viridis \
    uwot \
    ggbeeswarm \
    gridExtra \
    mlr3 \
    mlr3measures \
    caret

# single cell analysis tools

 rm -rf /tmp/downloaded_packages