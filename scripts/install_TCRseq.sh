#!/bin/bash

## build ARGs
NCPUS=${NCPUS:--1}

#set -e
#apt-get update -qq && apt-get -y --no-install-recommends install \
#  rm -rf /var/lib/apt/lists/*


install2.r --error --skipinstalled -n $NCPUS \
    Seurat \
    shazam \
    alakazam

# single cell analysis tools
installBioc.r \
    RSeqAn \
    scDblFinder \
    scater \
    scran \
    monocle3 \
    bluster \
    SingleR \
    enrichR \
    celldex

installGithub.r \
    JulianSpagnuolo/TCRdist

# annotation tools
installBioc.r \
    GEOquery \
    AnnotationDbi \
    org.Hs.eg.db \
    rentrez \
    biomaRt \
    Biostrings

    



 rm -rf /tmp/downloaded_packages